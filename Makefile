# which files to compile
FILES = src/main.c

# where to put and how to name the exe
EXE = bin/tetris

# which SDL libraries to include
SDL2_INC = lib/SDL2-2.0.14/x86_64-w64-mingw32/include/SDL2
SDL2_LIB = lib/SDL2-2.0.14/x86_64-w64-mingw32/lib
SDL2_IMAGE_INC = lib/SDL2_image-2.0.5/x86_64-w64-mingw32/include/SDL2
SDL2_IMAGE_LIB = lib/SDL2_image-2.0.5/x86_64-w64-mingw32/lib

# -w suppress all warnings
# -Wl,-subsystem,windows gets rid of console window
# COMPILER_FLAGS = -w -Wl,-subsystem,windows
COMPILER_FLAGS = -Wall

# libraries we are linking against
LINKER_FLAGS = -lSDL2main -lSDL2 -lSDL2_image

linux:
	gcc $(FILES) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(EXE)

windows:
	gcc $(FILES) -I$(SDL2_INC) -I$(SDL2_IMAGE_INC) -L$(SDL2_LIB) -L$(SDL2_IMAGE_LIB) $(COMPILER_FLAGS) -lmingw32 $(LINKER_FLAGS) -o $(EXE)