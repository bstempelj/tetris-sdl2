#if defined(_WIN32) || defined(WIN32)
    #include <SDL.h>
#else
    #include <SDL2/SDL.h>
#endif
#include <stdio.h>
#include <stdbool.h>
#include "blocks.h"

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define NUM_BLOCKS 2

typedef struct
{
    int x, y;
    int type;
    int state;
    bool active;
} Block;

SDL_Window* g_window;
SDL_Renderer* g_renderer;

int g_block_type = IBLOCK;
int g_block_state = BLOCK_STATE_UP;
Block* g_current_block = NULL;

void CircleBlockState()
{
    if (++g_current_block->state >= BLOCK_STATE_TOTAL)
    {
        g_current_block->state = BLOCK_STATE_UP;
    }
}

bool Init()
{
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! %s\n", SDL_GetError());
        return false;
    }

    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"))
    {
        printf("Warning: Linear texture filtering not enabled!");
    }

    g_window = SDL_CreateWindow(
        "Tetris",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        SDL_WINDOW_SHOWN);

    if (g_window == NULL)
    {
        printf("Window could not be created! %s\n", SDL_GetError());
        return false;
    }

    g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (g_renderer == NULL) {
        printf("Renderer could not be created! %s\n", SDL_GetError());
        return false;
    }
    SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);

    return true;
}

void Close()
{
    SDL_DestroyRenderer(g_renderer);
    SDL_DestroyWindow(g_window);
    g_window = NULL;
    g_renderer = NULL;

    SDL_Quit();
}

void FillRect(int x, int y)
{
    SDL_Rect fill_rect = {x, y, 20, 20};
    SDL_RenderFillRect(g_renderer, &fill_rect);
}

void DrawBlockState(int coords[8], int x, int y)
{
    for (int i = 0; i < 8; i += 2)
    {
        FillRect(x + coords[i], y + coords[i+1]);
    }
}

void DrawBlock(int block, int state, int x, int y)
{
    switch (block)
    {
    case OBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0x00, 0x00, 0xFF);
        FillRect(x, y);
        FillRect(x, y+21);
        FillRect(x+21, y);
        FillRect(x+21, y+21);
        break;

    case IBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0x00, 0xFF, 0x00, 0xFF);
        DrawBlockState(iblock_coords[state], x, y);
        break;

    case JBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0x00, 0x00, 0xFF, 0xFF);
        DrawBlockState(jblock_coords[state], x, y);
        break;

    case LBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0x00, 0xFF);
        DrawBlockState(lblock_coords[state], x, y);
        break;

    case SBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0x00, 0xFF, 0xFF, 0xFF);
        DrawBlockState(sblock_coords[state], x, y);
        break;

    case ZBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0x00, 0xFF, 0xFF);
        DrawBlockState(zblock_coords[state], x, y);
        break;

    case TBLOCK:
        SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
        DrawBlockState(tblock_coords[state], x, y);
        break;
    }
}

void HandleEvents(SDL_Event *event, bool *quit)
{
    while (SDL_PollEvent(event) != 0)
    {
        switch (event->type) {
            case SDL_KEYDOWN:
                switch (event->key.keysym.sym) {
                    case SDLK_UP:
                        CircleBlockState();
                        break;

                    case SDLK_RIGHT:
                        g_current_block->x += 20;
                        break;

                    case SDLK_DOWN:
                        g_current_block->y = SCREEN_HEIGHT - 40;
                        break;

                    case SDLK_LEFT:
                        g_current_block->x -= 20;
                        break;

                    case SDLK_q:
                        *quit = true;
                        break;
                }
                break;

            case SDL_QUIT:
                *quit = true;
                break;
        }
    }
}

void Render(Block blocks[], int num_blocks)
{
    SDL_SetRenderDrawColor(g_renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(g_renderer);

    for (int i = 0; i < num_blocks; i++)
    {
        DrawBlock(blocks[i].type, blocks[i].state, blocks[i].x, blocks[i].y);
    }
    SDL_RenderPresent(g_renderer);
}

Block CreateBlock(int type, int x, int y)
{
    Block block;
    block.x = x;
    block.y = y;
    block.type = type;
    block.state = BLOCK_STATE_UP;
    block.active = true;
    return block;
}

int main(int argc, char* args[])
{
    SDL_Event event;
    bool quit = false;

    Block blocks[NUM_BLOCKS];
    blocks[0] = CreateBlock(TBLOCK, SCREEN_WIDTH/2 - 31, 1);
    g_current_block = &blocks[0];

    if (!Init())
    {
        printf("Failed to initialize SDL!\n");
        return 1;
    }

    while(!quit)
    {
        HandleEvents(&event, &quit);
        Render(blocks, NUM_BLOCKS);
    }

    Close();
    return 0;
}