#pragma once

enum BlockTypes
{
    OBLOCK,
    IBLOCK,
    JBLOCK,
    LBLOCK,
    SBLOCK,
    ZBLOCK,
    TBLOCK,
    BLOCKS_TOTAL
};

enum BlockStates
{
	BLOCK_STATE_UP,
	BLOCK_STATE_RIGHT,
	BLOCK_STATE_DOWN,
	BLOCK_STATE_LEFT,
	BLOCK_STATE_TOTAL
};

int iblock_coords[4][8] = {
    // up
    {0,0, 0,21, 0,42, 0,63},
    // right
    {0,0, 21,0, 42,0, 63,0},
    // down (same as up)
    {0,0, 0,21, 0,42, 0,63},
    // left (same as right)
    {0,0, 21,0, 42,0, 63,0},
};

int jblock_coords[4][8] = {
    // up
    {21,0, 21,21, 0,42, 21,42},
    // right
    {0,0, 0,21, 21,21, 42,21},
    // down
    {0,0, 21,0, 0,21, 0,42},
    // left
    {0,0, 21,0, 42,0, 42,21}
};

int lblock_coords[4][8] = {
    // up
    {0,0, 0,21, 0,42, 21,42},
    // right
    {0,21, 21,21, 42,21, 0,42},
    // down
    {0,0, 21,0, 21,21, 21,42},
    // left
    {0,21, 21,21, 42,21, 42,0}
};

int sblock_coords[4][8] = {
    // up
    {0,0, 0,21, 21,21, 21,42},
    // right
    {0,21, 21,21, 21,42, 42,42},
    // down (same as up)
    {0,0, 0,21, 21,21, 21,42},
    // left (same as right)
    {0,21, 21,21, 21,42, 42,42},
};

int zblock_coords[4][8] = {
    // up
    {21,0, 21,21, 0,21, 0,42},
    // right
    {21,21, 42,21, 0,42, 21,42},
    // down (same as up)
    {21,0, 21,21, 0,21, 0,42},
    // left (same as right)
    {21,21, 42,21, 0,42, 21,42},
};

int tblock_coords[4][8] = {
    // up
    {21,0, 0,21, 21,21, 42,21},
    // right
    {21,0, 21,21, 42,21, 21,42},
    // down
    {0,21, 21,21, 42,21, 21,42},
    // left
    {21,0, 0,21, 21,21, 21,42}
};